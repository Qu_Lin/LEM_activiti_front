/*
 * 该文件存放项目全局变量
 */


/**
 * 子导航背景色
 * @type {string}
 */
export const child_nav_bg_color = "#425067";

/**
 * 后端服务IP
 * @type {string}
 */
export const SERVICE_IP = "#";
