/**
 * 验证手机号码
 * @param phoneNum
 * @returns {Promise<any>}
 */
export function validatePhoneNum(phoneNum) {
  return new Promise((resolve, reject) => {
    const filter = /^1[3|4|5|7|8][0-9]\d{8}$/;
    filter.test(phoneNum) ? resolve() : reject();
  });
};

/**
 * 验证邮件
 * @param email
 * @returns {Promise<any>}
 */
export function validateEmail(email) {
  return new Promise((resolve, reject) => {
    const filter = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    filter.test(email) ? resolve() : reject();
  });
}

/**
 * 验证网关ID
 * @param id
 * @returns {Promise<any>}
 */
export function validateGWID(id) {
  let tip = "";
  let pass = true;
  return new Promise((resolve, reject) => {
    if (id.length > 8) {
      tip = "网关ID的长度不能大于8位";
      pass = false;
    } else {
      const filter = /^[a-eA-E0-9]{1,8}$/;
      if (!filter.test(id)) {
        tip = "网关ID应由0-9a-eA-E组成";
        pass = false;
      }
    }

    if (!pass) reject(tip);
    resolve(pass);
  });
}
