// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import store from "./store/index"

import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import Vuex from 'vuex'
import VueProgressBar from 'vue-progressbar'
import moment from "moment"

Vue.use(ElementUI);
Vue.use(Vuex);

Vue.config.productionTip = false

// progressBar
Vue.use(VueProgressBar, {
  color: '#3fb984',
  failedColor: 'red',
  height: '2px'
});

// 设置时间显示方式
moment.locale("zh-cn");

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  components: {
    App
  },
  template: '<App/>'
})
