import mainPage from "./pages/mainPage"

/**
 * 主页路由配置
 */
import homePageRouterConfig from "@/components/subPages/homePage/homePageRouterConfig.js"

/**
 * 业务管理路由配置
 */
import bmPageRouterConfig from "@/components/subPages/businessManagementPage/bmPageRouterConfig.js"

/**
 * 业务历史路由配置
 */
import bhPageRouterConfig from "@/components/subPages/businessHistoryPage/bhPageRouterConfig.js"


export default [{
  path: "/mainPage",
  component: mainPage,
  redirect: "/mainPage/homePage/home",
  children: [
    homePageRouterConfig,
    bmPageRouterConfig,
    bhPageRouterConfig
  ]
}]
