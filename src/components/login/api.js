import * as apis from "api/api.js"
import {SERVICE_IP} from "common/js/globalVar.js"

/**
 * Post方法
 * @param url
 * @param par
 * @returns {AxiosPromise<any>}
 * @constructor
 */
export function Post(url, par) {
  return apis.Post(SERVICE_IP + url, par);
}
