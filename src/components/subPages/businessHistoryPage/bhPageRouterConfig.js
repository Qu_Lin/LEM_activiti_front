const bhPageRouterDis = () =>
  import ('./pages/bhPageRouterDis.vue')
const bhPage = () =>
  import ('./pages/bhPage.vue')

export default {
  path: "bhPageRouterDis",
  component: bhPageRouterDis,
  redirect: "bhPageRouterDis/bhPage",
  children: [{
    path: "bhPage",
    component: bhPage
  }]
}
