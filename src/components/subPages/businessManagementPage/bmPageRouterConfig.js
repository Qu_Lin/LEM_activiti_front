const bmPageRouterDis = () =>
  import ('./pages/bmPageRouterDis.vue')
const bmPage = () =>
  import ('./pages/bmPage.vue')

export default {
  path: "bmPageRouterDis",
  component: bmPageRouterDis,
  redirect: "bmPageRouterDis/bmPage",
  children: [{
    path: "bmPage",
    component: bmPage
  }]
}
