const homePageRouterDis = () =>
  import ('./pages/homePageRouterDis.vue')
const home = () =>
  import ('./pages/homePage.vue')
const innerPage = () =>
  import('./pages/innerPage.vue')

export default {
  path: "homePage",
  component: homePageRouterDis,
  redirect: "homePage/home",
  children: [{
    path: "home",
    component: home
  },{
    path: 'innerPage',
    component: innerPage
  }]
}
