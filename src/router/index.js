import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/**
 * 主页 路由
 */
import homeBaseRouter from "components/home/mainPageRouterConfig.js"


const router = new Router({
  routes: [{
      path: "/",
      redirect: "/login",
    },
    {
      path: "/login",
      component: resolve => require(['components/login/pages/index.vue'], resolve),
      meta: {
        notToken: true
      }
    },
    ...homeBaseRouter,
    // {
    //   path: "*",
    //   component: error404
    // },
    // {
    //   path: "/403",
    //   component: error403
    // }
  ]
})

/**
 * vue-router 拦截
 */
// router.beforeEach((to, from, next) => {
//   // 登录过滤
//   if (process.env.LOGIN_FILTER) {
//     // 需要token才能访问指定页面
//     if (to.meta.notToken) {
//       next();
//     } else {
//       if (store.state.home.profile.token) {
//         next();
//       } else {
//         next({
//           path: "/login"
//         });
//       }
//     }
//   } else {
//     next();
//   }
// });

export default router;
