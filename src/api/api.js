/*
 * 基础的网络请求工具
 */
import axios from "axios"
// import Qs from "qs"
import Vue from "vue"
// import store from "@/store/index.js"
import router from "@/router/index.js"

// import {SERVER_API_ADDR} from "common/js/globalVar"

// 设置axios默认地址
// axios.defaults.baseURL = `${SERVER_API_ADDR}/api`;
// axios.defaults.baseURL = `${SERVER_API_ADDR}`;

// 设置axios超时时间
axios.defaults.timeout = 10000;

// 设置axios默认Content-type类型
axios.defaults.headers.post["Content-Type"] = "application/json;charset=UTF-8";
axios.defaults.withCredentials = true;

//添加请求拦截器
axios.interceptors.request.use(config => {
  // 请求开始，显示进度条
  const progressBar = (new Vue()).$Progress;
  progressBar.start();

  // if (store.state.home.profile.token) {
  //   config.headers["x-user-token"] = `${store.state.home.profile.token}`;
  // }

  return config
}, error => {
  return Promise.reject(error)
})

//添加响应拦截器
axios.interceptors.response.use(response => {
  // 请求成功，关闭进度条
  const progressBar = (new Vue()).$Progress;
  progressBar.finish();

  // 获取返回的data
  const data = response.data;
  //操作成功
  if ((data.loginStatus === 1) || (data.loginStatus === 0 && data.resCode === 1)) {
    return data;
  } else if (data.resCode === "40101") {
    const message = (new Vue()).$message;
    const errorStr = "您的账号已过期，请重新登录";
    message({
      type: "error",
      message: errorStr
    });
    router.push("/login");
  } else {
    // 报错  抛出异常
    const message = (new Vue()).$message;
    message({
      type: "error",
      message: data.message ? data.message : "出错啦，请稍后重试"
    });
    throw data.message;
  }

}, error => {
  console.log(error);
  let errorStr = "服务器出错啦，请稍后重试";
  const message = (new Vue()).$message;
  // 请求错误，显示进度条
  const progressBar = (new Vue()).$Progress;
  progressBar.fail();

  // 报错
  message({
    type: "error",
    message: errorStr
  });

  return Promise.reject(errorStr);
});

/**
 * Post方法
 * @param url
 * @param par
 * @returns {AxiosPromise<any>}
 * @constructor
 */
export function Post(url, par) {
  return axios.post(url, par);
}

/**
 * 上传文件
 * @param url
 * @param file
 * @returns {AxiosPromise<any>}
 * @constructor
 */
// export function Upload(url, file) {
//   const form = new FormData();
//   form.append("file", file);
//   return axios.post(url, form, {
//     headers: {'Content-Type': 'multipart/form-data'}
//   });
// }

/**
 * Get方法
 * @param url
 * @param par
 * @returns {AxiosPromise<any>}
 * @constructor
 */
export function Get(url, par, payload) {
  return axios.get(url, {
    params: par,
    data: payload
  });
}

/**
 * Delete方法
 * @param url
 * @param par
 * @returns {AxiosPromise}
 * @constructor
 */
export function Del(url, par, payload) {
  return axios.delete(url, {
    params: par,
    data: payload
  });
}

/**
 * Put方法
 * @param url
 * @param par
 * @returns {AxiosPromise<any>}
 * @constructor
 */
export function Put(url, par, payload) {
  return axios.put(url, par, {
    data: payload
  })
}

/**
 * 下载文件
 * @param url
 * @constructor
 */
// export function Download(url) {
//   window.open(url);
// }
