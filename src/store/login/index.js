import loginStates from "./states";
import loginMutations from "./mutations";
import * as loginGetters from "./getters";
import * as loginActions from "./actions";

export default {
  state: loginStates,
  mutations: loginMutations,
  actions: loginActions,
  getters: loginGetters
};
