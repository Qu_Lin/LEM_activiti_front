import * as types from "./mutation_types"
import * as api from "@/components/login/api.js"

/**
 * 获取当前登录用户离线消息
 * @param commit
 * @param userName
 */
export const userLogin = function ({
  commit
}, loginInfo) {
  api.Post('/iotapplication/user/login', {
    userName: loginInfo.userName,
    password: loginInfo.password
  }).then(res => {
    if (res.resCode === 1) {
      commit(types.SET_CURRENT_USERINFO, res ? res : []);
    }
  }).catch(err => {
    commit(types.SET_CURRENT_USERINFO, []);
  });
}
