import * as types from "./mutation_types"
import Vue from "vue"

export default {
  //同步当前登录用户信息
  [types.SET_CURRENT_USERINFO](state, data) {
    state.userInfo = data;
  }
};
