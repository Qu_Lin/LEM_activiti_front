/**
 * 当前登录用户信息
 */
export const currentUser = state => state.userInfo;
