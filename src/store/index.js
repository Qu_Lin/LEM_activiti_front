import Vue from "vue"
import Vuex from "vuex"
// vuex持久化插件
import createPersistedState from 'vuex-persistedstate'

/**
 * login模块对应的Vuex
 * @type {{state, mutations, actions, getters}}
 */
import login from "./login/index"


Vue.use(Vuex);

/**
 * 是否调试模式
 * @type {boolean}
 */
const debug = process.env.NODE_ENV !== "production";


export default new Vuex.Store({
  modules: {
    login
  },
  strict: debug,
  plugins: [createPersistedState()]
});
